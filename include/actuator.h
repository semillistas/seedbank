#ifndef ACTUATOR_H
#define ACTUATOR_H

#include <string>
#include <Arduino.h>

namespace SeedBank{
enum class ACTUATOR_TYPE { NONE, RELAY, SOLID, PWM };
ACTUATOR_TYPE getActuatorType(String type);

class Actuator {
public:
    Actuator() {};
    virtual ~Actuator() = default;
    virtual void enable() = 0;
    virtual void disable() = 0;

    virtual void set_value(float value);
    virtual float get_value();

    String get_type();
    bool is_active();

    void send_state();

    void set_safety_time(unsigned int millis) { m_safety_time_millis = millis; }

protected:
    ACTUATOR_TYPE m_type = ACTUATOR_TYPE::NONE;
    int m_pin = -1;
    bool m_is_active = false;
    bool m_is_invert = false;
    float m_value = 0.0f;

    String m_name = "";
    bool m_last_change_published = false;

    unsigned int m_safety_time_millis = 0;
    unsigned long m_last_changed_state_time = 0;
};

class Relay : public Actuator {
public:
    Relay(int pin, String name, bool invert);

    void enable();
    void disable();
};

class Solid : public Actuator {
public:
    Solid(int pin, String name, bool invert);

    void enable();
    void disable();
};

class SoftPwm : public Actuator {
public:
    SoftPwm(int pin, String name, bool invert);

    void enable() override;
    void disable() override;
    void set_value(float value) override;
    float get_value() override;

    void handlePWM(unsigned long deltatime);
    inline int frequency(){return interval_time/steps;};
    inline bool get_state_pin(){return m_state_pin;};
private:

    TaskHandle_t m_task;
   // float m_pwmValue;
    //bool  m_pwmState;
    int   m_pwmTickTime;
    bool m_state_pin = false;

    const int interval_time = 1000;
    const int steps = 100;
};
void loop_softPWM_task(void* p_softPWM);


class Pwm : public Actuator {
public:
    Pwm(int pin, String name, bool invert);

    void enable() override;
    void disable() override;
    void set_value(float value) override;
    float get_value() override;

private:

    // https://deepbluembedded.com/esp32-pwm-tutorial-examples-analogwrite-arduino/
    int m_pwm_channel = 0;
    int m_pwm_res = 13;
    int m_pwm_freq = 5000;
};

}

#endif
