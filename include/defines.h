#ifndef DEFINES_H
#define DEFINES_H

#define DEBUG_SERIAL
#ifdef DEBUG_SERIAL
    #define SERIAL_PRINT(arg) Serial.print(arg);
    #define SERIAL_PRINTLN(arg) Serial.println(arg);
#else
    #define SERIAL_PRINT(arg)
    #define SERIAL_PRINTLN(arg)
#endif

#define CONFIG_FILENAME "/config.json"

#define TEMP_ERROR_READING -1000.0

//TODO variables in config
#define INTERVAL_UPDATE_TIME  10000
#define INTERVAL_SAFETY_RELAY 10000

#include <iostream>
#include <sstream>


template < typename Type > std::string to_str (const Type & t)
{
  std::ostringstream os;
  os << t;
  return os.str();
}
#endif
