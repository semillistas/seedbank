#ifndef STATE_H
#define STATE_H

#include "config.h"

namespace SeedBank{

class State {
public:
    State(Config *config);
    bool is_clock_set = false;
    bool is_mqtt_set = false;

    void sensors_update();
    void actuators_update();
    float get_avg_temperature();
    void print_all_temperatures();

private:
    Config *m_config;
};

}
#endif
