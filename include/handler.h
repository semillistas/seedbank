#ifndef HANDLER_H
#define HANDLER_H

#include "config.h"
#include "state.h"

namespace SeedBank{
class Handler {
public:
    Handler(Config* config, State* state);
    virtual ~Handler() = default;

    virtual void run(float deltatime) = 0;
    virtual void send_data(float deltatime);

protected:
    HANDLER_TYPE type;

    Config* m_config;
    State* m_state;
};

class Handler_test : public Handler {
public:
    Handler_test(Config* config, State* state);
    ~Handler_test();

    void run(float deltatime) override;
    virtual void send_data(float deltatime) override;
};

class Handler_basic : public Handler {
public:
    Handler_basic(Config* config, State* state);
    ~Handler_basic();

    void run(float deltatime) override;
    virtual void send_data(float deltatime) override;
};

class Handler_pid : public Handler {
public:
    Handler_pid(Config* config, State* state);
    ~Handler_pid();

    void run(float deltatime) override;
    virtual void send_data(float deltatime) override;
    void configure(float Kp, float Ki, float Kd, float maxIntegralError, float maxDiffError);

private:
    float previous_error = 0;
    float integral = 0;

    //TODO hacer tests con diferentes valores
    float maxIntegralError = 5000.0f;
    float maxDiffError = 3.0f;

    //TODO pasar las variables al UI html para poder hacer ajustes
    float Kp = 1.0f;
    float Ki = 0.01f;
    float Kd = 30.0f;
};


}
#endif
