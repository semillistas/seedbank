## Instalación

* Nosotros usamos la plataforma [PlatformIO](https://platformio.org/) para compilar los archivos. Instale la plataforma en su PC, dependiendo de su sistema operativo, siguiendo las siguientes opciones y instrucciones:

- [CLI](https://platformio.org/install/cli)

* Existe otro método posible que no explicamos:
- [IDE, integrated in vscode](https://platformio.org/install/ide?install=vscode)

## instrucciones para CLI

El método 1 sirve para la primera instalación o para actualizaciones futuras.

El método 2 sólo sirve para actualizaciones.

### Método 1.- Compile & Upload

* Se descarga el proyecto al PC. Desde la carpeta donde estén descargados se ejecutan los siguientes comandos en el terminal linux para compilar y subir el proyecto. Previamente conecte al PC la ESP32 por USB.


```bash
pio run
pio run -t upload

pio run -t buildfs
pio run -t uploadfs
```

### Método 2.- Update by OTA


- Compile the firmware

    ```bash
    pio run
    ```

- File in .pio/build/{board-name}/firmware.bin
- Build the file fs

    ```bash
    pio run -t buildfs
    ```

- File in .pio/build/{board-name}/spiffs.bin
- Upload in page <http://{ip}/update>
