## Material necesario

* [Especificaciones y modificaciones del horno](doc/horno.md)

* ESP32

![esp32 developement board](esp32_30pin.jpg "ESP32"){width=600}

* [Rele de estado solido para 220V/25A max, input 5Vdc, output 220Vac](https://tienda.bricogeek.com/interruptores/1548-rele-de-estado-solido-25a-ssr.html)

* Transistor 2n2222
* Resistencia 1kΩ
* Diodo 1N4004

* [Sensor PT100, de 3 cables.](https://www.amazon.es/gp/product/B07NC73MSP/) Usar PT100 de 4 cables para menor error.

* [Controladora PT100, MAX31865](https://www.amazon.es/gp/product/B09Z2B2FG5/)


## Conexiones
Esquema general de conexiones entre los distintos elementos

![conexiones entre elementos](conexionado_horno.jpg "conexiones"){width=800}


## Conexión del Rele a la ESP32
* Conectamos la señal de entrada del transistor al GPIO27 del ESP32.

![Transistor schematic](circuito_transistor.jpeg "Transistor connection example"){width=400}

## Configuración de la controladora MAX31865

* Para una PT100 de 3 cables seguir estas instrucciones:

Suelde el puente etiquetado 2/3 Wire.
Corte la unión que conecta el lado izquierdo del puente de 2 vías justo encima de Rref y luego suelde el lado derecho etiquetado 3.

![soldar jumpers](adafruit_products_jumpers.jpg "soldar jumpers"){width=400}

* Para más información o uso de PT100 de 4 cables veáse [enlace](https://learn.adafruit.com/adafruit-max31865-rtd-pt100-amplifier/pinouts#configuration-jumpers-2928716)


## Conexion de la controladora MAX31865 a la ESP32

* El MAX31865 va conectado por SPI:
* CLK --> GPIO18
* SDI --> GPIO23 MOSI
* SDO --> GPIO19 MISO
* CS  --> GPIO2
* GND y VIN (5V) a los pines de la fuente de alimentación dc


## Fotos prototipo


![Transistor detail](horno_prototipo_transistor.jpg "Transistor detail"){height=400}
![Horno prototipe](horno_prototipo.jpg "Prototipe"){height=400}
