## Especificaciones

* Horno domestico grande (60-70L) o pequeño (20L)
* Ventilador interno.
* Potencia: sólo se usa la resistencia inferior. En los hornos grandes serán 1200W y en los pequeños 700W. Se podría cambiar la resistencia del horno por otra de menor potencia.
* Sin electrónica de control. Debería tener los típicos selectores giratorios (temperatura, resistencia y ventilador).
* Bandejas internas de rejilla para facilitar la movilidad del aire.   

## Modificaciones

* Independizar el encendido del ventilador para que sea manual. Sacar los dos cables del ventilador de dentro del horno y ponerle un interruptor en la parte frontal del horno.
* El encendido/apagado del horno va a estar controlado por el relé de estado sólido. Los dos cables internos del horno que controlen la tensión de las resistencias, pero que no interrumpan la tensión del ventilador, son los que se envían al relé. Se recomienda hacer de manera que los selectores giratorios sigan funcionando (exceptuando el ventilador, que lo independizamos).
* Agujerear con broca el horno para introducir el sensor PT100.
