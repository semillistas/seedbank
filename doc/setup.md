Pasos para configurar en la página "setup"


## wifi

- working mode: 'normal'
Cuando semillistas avance en el software aquí se podrá elegir entre modo "horno", "nevera" y otros dispositivos que estamos diseñando.

- SSID: nombre de la wifi de nuestro laboratorio

- Password: contraseña del wifi

Al introducir esta información, para acceder a la página de la ESP32 no lo haremos a través de la IP por defecto, sino que tendremos que averiguar la IP que el router a asisnado a la ESP32 (ver "otras consideraciones" en readme.md)


## mqtt server (opcional)

- Server: dirección IP del servidor
- Port: 1883 (el técnico de la instalación debe asegurarse)
- Username: administrador del servidor
- Password: contraseña del administrador

Este apartado es opcional porque el horno funcionará sin él. El mqtt server sirve para monitorizar la temperatura en tiempo real del horno. Además, estamos implementando en el servidor un software para visualización de los datos (futura mejora).


## Time

- NTP server:  pool.ntp.org
Este es el servidor que usamos para que la ESP32 esté en la hora correcta.

- NTP gwtoffset: desfase horario respecto al meridiano de Greenwich


## Sensors

- seleccionar sensor tipo "PT100" y el puerto: GPIO2
Este puerto es la salida hacia "CS" de la controladora de la PT100

Sólo hay que añadir un sensor en este apartado.


## heat

- seleccionar relé tipo "SOLID" y el puerto: GPIO27
Este puerto es la salida hacia el relé de estado sólido

- inverter signal: deberemos marcar esta casilla en caso de que cuando la ESP32 quiera encender la resistencia del horno, haga lo contrario, apagarla. Y viceversa. Tendremos que observar el funcionamiento para decidirlo. 


## FAN  COLD  LIGHT

Son opciones de setting para las neveras.
Para el horno hay que dejarlas en "NONE"

## Schedule

- Indicar horario de funcionamiento del horno, de 00:00 a 00:00 para mantener la temperatura estable 24 horas. Los métodos de secado de semillas duran 17h, pero lo controlamos de forma manual.

- Poner la temperatura que tiene que mantener el horno

## Handler

- seleccionar la opción "PID"

### PID parameters

Ajustar los valores por defecto para hasta minimizar la oscilación de la temperatura.
Valores por defecto:
- Kp    1       
- Ki    0.01    
- Kd    30      

Ejemplo para horno pequeño:
- Kp    1
- Ki    0.05
- Kd    45

Ejemplo para horno grande:
- Kp    1
- Ki    0.001
- Kd    40

- maxIntegralError: 5000
- maxDiffError: 3
Estos dos últimos parámetros resetean la memoria del PID para no acumular errores. En principio no deben tocarse los valores por defecto.

![Ejemplo](doc/1704826538392.png "Ejemplo de ajuste de la oscilación"){width=600}




