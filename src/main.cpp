#include <Arduino.h>
#include <SPIFFS.h>

#include "defines.h"
#include "config.h"
#include "state.h"
#include "display.h"
#include "webserver.h"
#include "decisor.h"

namespace SeedBank{
Config *config;
State *state;
Display *display;
WebServer *webserver;
Decisor *decisor;
}


void setup() {
    // Initialize hardware
    Serial.begin(115200);
    SERIAL_PRINTLN("Main::Boot");

    SERIAL_PRINTLN("Main::Setup SPIFFS");
    SPIFFS.begin();

    // Read configuration from Flash
    SeedBank::config = new SeedBank::Config();

    // Create the Initial State based on configuration
    SeedBank::state = new SeedBank::State(SeedBank::config);

    // Lauch Display task
    SeedBank::display = new SeedBank::Display(SeedBank::config, SeedBank::state);

    // Lauch WebServer task
    SeedBank::webserver = new SeedBank::WebServer(SeedBank::config, SeedBank::state);

    // Lauch Decisor task
    SeedBank::decisor = new SeedBank::Decisor(SeedBank::config, SeedBank::state);
}

void loop() {
    SeedBank::display->loop();
    delay(1000);
}
