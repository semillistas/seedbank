#include "sensor.h"
#include "webserver.h"

namespace SeedBank{
// for send mqtt data
extern WebServer *webserver;
SENSOR_TYPE getSensorType(String type){
    if(type == "DHT22"){
        return SENSOR_TYPE::DHT22;
    }
    else if (type == "DS18B20") {
        return SENSOR_TYPE::DS18B20;
    }
    else if (type == "BMP280") {
        return SENSOR_TYPE::BMP280;
    }
    else if (type == "HDC2080") {
        return SENSOR_TYPE::HDC2080;
    }
    else if (type == "DUMMY") {
        return SENSOR_TYPE::DUMMY;
    }
    else if (type == "DOOR") {
        return SENSOR_TYPE::DOOR;
    }
    else if (type == "BUTTON") {
        return SENSOR_TYPE::BUTTON;
    }
        else if (type == "PT100") {
        return SENSOR_TYPE::PT100;
    }

    return SENSOR_TYPE::NONE;
}

Sensor::Sensor() {}
float Sensor::get_value() { return this->m_last_value;}

bool Sensor::is_temperature()
{
    switch (m_type)
    {
    case SENSOR_TYPE::DHT22:
    case SENSOR_TYPE::DS18B20:
    case SENSOR_TYPE::BMP280:
    case SENSOR_TYPE::HDC2080:
    case SENSOR_TYPE::PT100:
      return true;
    case SENSOR_TYPE::DUMMY:
    case SENSOR_TYPE::DOOR:
    case SENSOR_TYPE::BUTTON:
    default:
        return false;
    }
}

String Sensor::get_type_string(){
    switch (m_type)
    {
        case SENSOR_TYPE::DHT22:
            return "DHT22";
        case SENSOR_TYPE::DS18B20:
            return "DS18B20";
        case SENSOR_TYPE::BMP280:
            return "BMP280";
        case SENSOR_TYPE::HDC2080:
            return "HDC2080";
        case SENSOR_TYPE::DUMMY:
            return "DUMMY";
        case SENSOR_TYPE::DOOR:
            return "DOOR";
        case SENSOR_TYPE::BUTTON:
          return "BUTTON";
        case SENSOR_TYPE::PT100:
          return "PT100";
        default:
            return "unknow";
    }

}
// --------------------------------------------------------------------------
// Sensor DS18B20: Maxim, 1-Wire interfaz, +/- 0.5 ºC
// --------------------------------------------------------------------------
Sensor_DS18B20::Sensor_DS18B20(int pin) {
    int i = 0;

    this->m_type = SENSOR_TYPE::DS18B20;
    this->m_pin = pin;
    this->m_bus = new OneWire(this->m_pin);
    this->m_sensors = new DallasTemperature(this->m_bus);
    this->m_sensors->begin();
    while ((i < DS18B20_MAX_COUNT) && (this->m_sensors->getAddress(this->m_addresses[i], i) == true)) {
        this->m_sensors->setResolution(this->m_addresses[i], DS18B20_TEMP_PRECISION);
        i++;
    }
    this->m_count = i;
}

Sensor_DS18B20::~Sensor_DS18B20() {
    if (this->m_bus != nullptr) {
        delete this->m_bus;
        this->m_bus = nullptr;
    }
    if (this->m_sensors != nullptr) {
        delete this->m_sensors;
        this->m_sensors = nullptr;
    }
}

int Sensor_DS18B20::get_count(void) {
    return this->m_count;
}

float Sensor_DS18B20::get_value_by_index(int index) {
    this->m_sensors->requestTemperaturesByAddress(this->m_addresses[index]);
    return this->m_get_value_by_index(index);
}

float Sensor_DS18B20::m_get_value_by_index(int index) {
    return this->m_sensors->getTempC(this->m_addresses[index]);
}

void Sensor_DS18B20::update(void) {
    int count = this->m_count;
    float temperatures = 0.0;

    this->m_sensors->requestTemperatures();
    for (int i = 0; i < count; i++) {
        temperatures += this->m_get_value_by_index(i);
    }
    if (count > 0) {
        this->m_last_value = temperatures / count;
    }
    this->m_last_value = TEMP_ERROR_READING;
}

// --------------------------------------------------------------------------
// Sensor HDC2080: Texas Instruments, i2C interfaz (0x40, 0x41), +/- 0.2 ºC
// --------------------------------------------------------------------------
Sensor_HDC2080::Sensor_HDC2080(int address) {
    this->m_type = SENSOR_TYPE::HDC2080;
    this->m_pin = address;
    reload();
}

Sensor_HDC2080::~Sensor_HDC2080() {
    if (this->m_sensor != nullptr) {
        delete this->m_sensor;
        this->m_sensor = nullptr;
    }
}

void Sensor_HDC2080::reload()
 {
    if(m_sensor != nullptr){
        delete m_sensor;
    }
    m_sensor = new HDC2080(this->m_pin);
    m_sensor->begin();
    m_sensor->reset();
    m_sensor->setHighTemp(48);
    m_sensor->setLowTemp(2);
    m_sensor->setHighHumidity(95);
    m_sensor->setLowHumidity(10);
    m_sensor->setMeasurementMode(TEMP_AND_HUMID);
    m_sensor->setRate(ONE_HZ);
    m_sensor->setTempRes(FOURTEEN_BIT);
    m_sensor->setHumidRes(FOURTEEN_BIT);
    m_sensor->triggerMeasurement();
 }

void Sensor_HDC2080::update() {
    float t = this->m_sensor->readTemp();
    if (t == -40.0) {
        t = TEMP_ERROR_READING;
        reload();
    }

    auto currentTime = millis();
    if (webserver != nullptr && (currentTime - m_last_send_state_time) > INTERVAL_UPDATE_TIME) {
        if (t != TEMP_ERROR_READING) {
            if (webserver != nullptr &&
                webserver->mqtt_publish("sensors/temp/" + String( this->m_pin ), String(round(t * 100) / 100))) {
                m_last_send_state_time = currentTime;
            }
        }
    }
    this->m_last_value = t;

}


// --------------------------------------------------------------------------
// Sensor BMP280: Bosch, i2C interfaz (0x76, 0x77), +/- 0.5 ºC
// --------------------------------------------------------------------------
Sensor_BMP280::Sensor_BMP280(int address) {
    this->m_type = SENSOR_TYPE::BMP280;
    this->m_pin = address;
    this->m_sensor = new Adafruit_BMP280();
    if (!this->m_sensor->begin(this->m_pin)) {
        SERIAL_PRINT("Could not find a BMP280 sensor at address ");
        SERIAL_PRINTLN(this->m_pin);
    }
    this->m_sensor->setSampling(
        Adafruit_BMP280::MODE_NORMAL,     // Operating Mode.
        Adafruit_BMP280::SAMPLING_X2,     // Temp. oversampling
        Adafruit_BMP280::SAMPLING_X16,    // Pressure oversampling
        Adafruit_BMP280::FILTER_X16,      // Filtering.
        Adafruit_BMP280::STANDBY_MS_500   // Standby time.
    );
}

Sensor_BMP280::~Sensor_BMP280() {
    if (this->m_sensor != nullptr) {
        delete this->m_sensor;
        this->m_sensor = nullptr;
    }
}

void Sensor_BMP280::update() {
    if (this->m_sensor == nullptr) {
        this->m_last_value = TEMP_ERROR_READING;
    }
    this->m_last_value = this->m_sensor->readTemperature();
}

// --------------------------------------------------------------------------
// Sensor DOOR: Generic input state sensor
// --------------------------------------------------------------------------
Sensor_DOOR::Sensor_DOOR(int pin) {
    m_pin = pin;
    m_type = SENSOR_TYPE::DOOR;
    m_last_change_published = false;
}

void Sensor_DOOR::update()
{
    auto previous = m_last_value;
    m_last_value = static_cast<float>(digitalRead(m_pin));
    if (previous != m_last_value || !m_last_change_published) {
        m_last_change_published = false;
        if (webserver != nullptr && webserver->mqtt_publish("sensors/door", String(m_last_value,0))){
            m_last_change_published = true;
        }
    }
}


// --------------------------------------------------------------------------
// Sensor BUTTON: Generic button sensor with interrupt callback
// --------------------------------------------------------------------------
Sensor_BUTTON::Sensor_BUTTON(int pin, void (*callback)(Sensor_BUTTON *button)) {
    // TODO
}

void Sensor_BUTTON::update() {
    // TODO
}

// --------------------------------------------------------------------------
// Sensor DUMMY: Testing proposes, returns random temp values between a range with a maximum variation
// --------------------------------------------------------------------------
Sensor_DUMMY::Sensor_DUMMY(float min, float max, float max_variation) {
    // TODO
}

void Sensor_DUMMY::update() {
    // TODO
}

// --------------------------------------------------------------------------
// Sensor PT100 via MAX31865 SPI interfaz
// --------------------------------------------------------------------------
Sensor_PT100::Sensor_PT100(int wires, int cs_pin) {

  this->m_type = SENSOR_TYPE::PT100;
  this->m_pin = cs_pin;
  m_sensor = new Adafruit_MAX31865(this->m_pin);
m_sensor->begin(MAX31865_3WIRE);
//   switch (wires) {
//   case 2:
//     m_sensor->begin(MAX31865_2WIRE);
//     break;
//   case 3:
//     m_sensor->begin(MAX31865_3WIRE);
//     break;
//   case 4:
//     m_sensor->begin(MAX31865_4WIRE);
//     break;
//   }
}

Sensor_PT100::~Sensor_PT100() {
  if (this->m_sensor != nullptr) {
    delete this->m_sensor;
    this->m_sensor = nullptr;
  }
}



void Sensor_PT100::update() {

  uint16_t rtd = m_sensor->readRTD();

  float ratio = rtd;
  ratio /= 32768;

//   Serial.print("Ratio = "); Serial.println(ratio,8);
//   Serial.print("Resistance = "); Serial.println(m_rref*ratio,8);
//   Serial.print("Temperature = "); Serial.println(thermo.temperature(m_rnominal, m_rref));
  float t = m_sensor->temperature(m_rnominal, m_rref);

  // Check and print any faults
  uint8_t fault = m_sensor->readFault();
  if (fault) {
        Serial.print("Fault 0x");
        Serial.println(fault, HEX);
        if (fault & MAX31865_FAULT_HIGHTHRESH) {
         Serial.println("RTD High Threshold");
        }
        if (fault & MAX31865_FAULT_LOWTHRESH) {
         Serial.println("RTD Low Threshold");
        }
        if (fault & MAX31865_FAULT_REFINLOW) {
         Serial.println("REFIN- > 0.85 x Bias");
        }
        if (fault & MAX31865_FAULT_REFINHIGH) {
         Serial.println("REFIN- < 0.85 x Bias - FORCE- open");
        }
        if (fault & MAX31865_FAULT_RTDINLOW) {
            Serial.println("RTDIN- < 0.85 x Bias - FORCE- open");
        }
        if (fault & MAX31865_FAULT_OVUV) {
            Serial.println("Under/Over voltage");
        }
        m_sensor->clearFault();
            //t = TEMP_ERROR_READING;
    }

    //if( abs(this->m_last_value - t) > 0.2 ){
        this->m_last_value = t;
    //}

    Serial.print("last value: ");
    Serial.println(this->m_last_value);

    auto currentTime = millis();
    if (webserver != nullptr && (currentTime - m_last_send_state_time) > INTERVAL_UPDATE_TIME) {
        if (t != TEMP_ERROR_READING) {
            bool send_ok = webserver->mqtt_publish("sensors/temp/" + String(this->m_pin), String(round(t * 100) / 100));
            if (webserver != nullptr && send_ok) {
                m_last_send_state_time = currentTime;
            }
        }
    }
}
}
