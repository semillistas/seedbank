#include "webserver.h"
#include "handler.h"

namespace SeedBank{
// for send mqtt data
extern WebServer* webserver;

Handler::Handler(Config* config, State* state) {
    this->m_config = config;
    this->m_state = state;
}

void Handler::send_data(float deltatime)
{
    static float last_update_time = 0;
    last_update_time += deltatime;

    if (last_update_time > INTERVAL_UPDATE_TIME)
    {
        webserver->mqtt_publish("info/avgTemp", String(round(m_state->get_avg_temperature() * 100) / 100));
        //webserver->mqtt_publish("info/refTemp", String(m_config->get_temperature_reference()));

        last_update_time = 0;
    }
}

Handler_basic::Handler_basic(Config* config, State* state) : Handler(config, state){}
Handler_basic::~Handler_basic() {}

void Handler_basic::run(float deltatime)
{
    float t_avg = m_state->get_avg_temperature();
    float t_reference = m_config->get_temperature_reference();

    float t_threshold = 0.25f;
    float t_max_diff = 0;
    for (auto s : m_config->sensors) {
        if (s->is_temperature()) {
            float t = s->get_value();
            t_max_diff = max(t_max_diff, abs(t - t_avg));
        }
    }

    if (m_config->cold != nullptr) {
        if ((t_reference + t_threshold) < t_avg) {
            m_config->cold->enable();
        } else {
            m_config->cold->disable();
        }
    }
    if (m_config->heat != nullptr) {
        if ((t_reference - t_threshold) > t_avg) {
            m_config->heat->enable();
        }
        else {
            m_config->heat->disable();
        }
    }
    if (m_config->fan != nullptr) {
        if (t_max_diff > 0.25f) {
            //this->m_config->fan->set_value(t_diff);
            m_config->fan->enable();
        }
        else {
            m_config->fan->disable();
        }
    }
    if (m_config->light != nullptr) {
        m_config->light->set_value(t_max_diff);
    }

    // // info for analyze algorithm
    // webserver->mqtt_publish("info/refTemp", String(t_reference));
    // webserver->mqtt_publish("info/maxDiffTemp", String(t_max_diff));
    // webserver->mqtt_publish("info/avgTemp", String(t_avg));
}

void Handler_basic::send_data(float deltatime) {
        Handler::send_data(deltatime);
}

Handler_test::Handler_test(Config* config, State* state) : Handler(config, state) {}
Handler_test::~Handler_test() {}
void Handler_test::run(float deltatime) {}
void Handler_test::send_data(float deltatime)
{
    Handler::send_data(deltatime);
}

Handler_pid::Handler_pid(Config* config, State* state) : Handler(config, state) {}
Handler_pid::~Handler_pid() {}
void Handler_pid::run(float deltatime) {
    deltatime /= 1000; //in seconds
    float t_avg = m_state->get_avg_temperature();
    float t_reference = m_config->get_temperature_reference();

    float error = t_reference - t_avg;
    float P = error;
    integral += (error * deltatime);

    // https://www.picuino.com/es/arduprog/control-pid-digital.html#control-anti-windup-integral
    if (abs(error) > maxDiffError || abs(integral) > maxIntegralError){
        integral = 0;
    }

    float D = (error - previous_error) / deltatime;
    float output = Kp * P + Ki * integral + Kd * D;
   // output = output / (5.0f * Kp); // scale for values between 0 and 1
    SERIAL_PRINTLN("  error:" + String(error) + "    P error:" + String(previous_error) );

    previous_error = error;

    if (m_config->cold != nullptr) {
        if (t_reference < t_avg) {
            if (output < -0.5f) {
                m_config->cold->enable();
            } else if(m_config->cold->is_active()){
                m_config->cold->disable();
            }
        }else{
            m_config->cold->disable();
        }
    }

    if (m_config->heat != nullptr) {

        if (output > 0) {
            m_config->heat->set_value(output);
        } else if (m_config->heat->is_active()) {
            m_config->heat->disable();
        }

        // if(t_avg > t_reference){
        //     m_config->heat->disable();
        // }
    }
    /*
    if (m_config->cold != nullptr) {
        if (t_reference < t_avg) {
            if (output < -0.5f) {
                m_config->cold->enable();
            } else if(m_config->cold->is_active()){
                m_config->cold->disable();
            }
        }else{
            m_config->cold->disable();
        }
    }

    if (m_config->heat != nullptr) {
        if(t_reference > t_avg){
            if (output > 0) {
                m_config->heat->set_value(output);
            } else if (m_config->heat->is_active()) {
                m_config->heat->disable();
            }
        }else{
            m_config->heat->disable();
        }
    }
    */

    if (m_config->fan != nullptr) {
        float t_max_diff = 0;
        for (auto s : m_config->sensors) {
            if (s->is_temperature()) {
                float t = s->get_value();
                t_max_diff = max(t_max_diff, abs(t - t_avg));
            }
        }
        if (t_max_diff > 0.25f) {
            //this->m_config->fan->set_value(t_diff);
            m_config->fan->enable();
        }
        else {
            m_config->fan->disable();
        }
    }

    SERIAL_PRINTLN("PID  P:"+String(P)+"   I:"+String(integral)+ "   D:"+String(D)+ "     OUTPUT:"+String(output) + "         dt:"+String(deltatime));

    //webserver->mqtt_publish("info/maxDiffTemp", String(t_max_diff));

    // info for analyze algorithm
    webserver->mqtt_publish("info/p", String(P,4));
    webserver->mqtt_publish("info/i", String(integral,4));
    webserver->mqtt_publish("info/d", String(D,4));

    webserver->mqtt_publish("info/output", String(output,4));
    webserver->mqtt_publish("info/error", String(error,4));
    webserver->mqtt_publish("info/Kp", String(Kp, 4));
    webserver->mqtt_publish("info/Ki", String(Ki, 4));
    webserver->mqtt_publish("info/Kd", String(Kd, 4));

    webserver->mqtt_publish("info/maxIntegralError", String(maxIntegralError, 4));
    webserver->mqtt_publish("info/maxDiffError", String(maxDiffError, 4));
}

void Handler_pid::send_data(float deltatime)
{
    Handler::send_data(deltatime);
}

void Handler_pid::configure(float _Kp, float _Ki, float _Kd,
                            float _maxIntegralError, float _maxDiffError){
  Kp = _Kp;
  Ki = _Ki;
  Kd = _Kd;
  maxIntegralError = _maxIntegralError;
  maxDiffError = _maxDiffError;
}
}
