
#include "timetable.h"

namespace SeedBank{

Timetable::Timetable(String start, String end, float value) {
    this->m_start = parse_hour(start);
    this->m_end = parse_hour(end);
    // if 00:00 convert to 24:00
    if(this->m_end == 0) this->m_end = 24 * 60;
    this->m_value = value;
}

Timetable::Timetable(int start, int end, float value) {
    this->m_start = start;
    this->m_end = end;
    // if 00:00 convert to 24:00
    if(this->m_end == 0) this->m_end = 24 * 60;
    this->m_value = value;
}

float Timetable::get_value() { return m_value; };

bool Timetable::is_in_interval() {
    tm time_info;
    getLocalTime(&time_info);
    int minutes_now = time_info.tm_hour * 60 + time_info.tm_min;
    if (m_start <= minutes_now && m_end >= minutes_now) {
        return true;
    }
    return false;
}

String Timetable::get_start_string()
{
    int hour = m_start / 60;
    int min = m_start - (hour*60);

    return String(to_str(hour).c_str()) + ":" + String(to_str(min).c_str());
}

String Timetable::get_end_string()
{
    int hour = m_end / 60;
    int min = m_end - (hour*60);

    return String(to_str(hour).c_str()) + ":" + String(to_str(min).c_str());
}


int parse_hour(String hour) {
    return (char_to_int(hour[0]) * 10 + char_to_int(hour[1])) * 60 + char_to_int(hour[3]) * 10 + char_to_int(hour[4]);
}

inline int char_to_int(char c) {
    return c - '0';
}
}
