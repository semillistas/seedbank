#include "config.h"
#include <SPIFFS.h>
#include <ArduinoJson.h>

namespace SeedBank{

Config::Config() {
    SERIAL_PRINTLN("Config::Setup");
    this->read();
}

Config::~Config() {
    SERIAL_PRINTLN("Config::Destroy");
}

String Config::defaultConfig() {
    return R"(
        {
            "sensors": [
                { "type": "PT100", "value" : 2 }
            ] ,
            "schedule" : [
                { "start": "00:00", "end" : "00:00", "value" : 20 }
            ] ,
            "mode" : "setup",
            "handler" : "test",
            "wifi_ssid" : "",
            "wifi_password" : "",
            "ntp_server" : "pool.ntp.org",
            "ntp_gmt_offset" : 2,
            "mqtt_server": "",
            "mqtt_port": 1883,
            "mqtt_username": "",
            "mqtt_password": "",
            "heat" :  { "type": "PWM", "value" : 27 },
            "cold" :  { "type": "NONE", "value" : 19 },
            "fan" :   { "type": "NONE", "value" : 18 },
            "light" : { "type": "NONE", "value" : 5 },
            "pid" : {
                "maxIntegralError": 5000.0,
                "maxDiffError": 3.0,
                "Kp": 1.0,
                "Ki": 0.01,
                "Kd": 30.0
                }
        }
    )";
}

void Config::reset() {
    File file = SPIFFS.open(CONFIG_FILENAME, FILE_WRITE);

    file.print(this->defaultConfig());
    file.close();
}

void Config::read() {
    StaticJsonDocument<2046> doc;
    DeserializationError error;
    File file;

    SERIAL_PRINTLN("Config::Reading JSON config from Flash");
    file = SPIFFS.open(CONFIG_FILENAME, FILE_READ);
    if (!file) {
        SERIAL_PRINTLN("Configuration file not found. Setting default config");
        this->reset();
        file = SPIFFS.open(CONFIG_FILENAME, FILE_READ);
    }

    SERIAL_PRINTLN("Config::Decoding JSON config");
    error = deserializeJson(doc, file);
    if (error || !doc.containsKey("mode")){
        SERIAL_PRINTLN("Configuration file is corrupted. Setting default config");
        this->reset();
        file = SPIFFS.open(CONFIG_FILENAME, FILE_READ);
        error = deserializeJson(doc, file);
    }
    file.close();

    if (error) {
        SERIAL_PRINT(F("Config::deserializeJson() failed: "));
        SERIAL_PRINTLN(error.f_str());
        return;
    }

    // Parse JSON configuration
    SERIAL_PRINTLN("Config::Parsing JSON config");
    this->m_mode = decode_json_key_as_mode(doc, "mode", MODE::SETUP);
    this->m_handler = decode_json_key_as_handler(doc, "handler", HANDLER_TYPE::TEST);

    this->m_ntp_server = decode_json_key_as_name(doc, "ntp_server", "pool.ntp.org");
    this->m_ntp_gmt_offset = decode_json_key_as_long(doc, "ntp_gmt_offset", 0);
    this->m_ntp_daylight_offset = decode_json_key_as_long(doc, "ntp_daylight_offset", 0);
    this->m_wifi_ssid = decode_json_key_as_name(doc, "wifi_ssid", "");
    this->m_wifi_password = decode_json_key_as_name(doc, "wifi_password", "");

    this->m_mqtt_server = decode_json_key_as_name(doc, "mqtt_server", "");
    this->m_mqtt_port = static_cast<int>(decode_json_key_as_long(doc, "mqtt_port", 1883));
    this->m_mqtt_username = decode_json_key_as_name(doc, "mqtt_username", "");
    this->m_mqtt_password = decode_json_key_as_name(doc, "mqtt_password", "");

    for (JsonObject repo : doc["sensors"].as<JsonArray>()) {
        if (strcmp(repo["type"].as<const char*>(), "DS18B20") == 0) {
            this->sensors.push_back(new Sensor_DS18B20(repo["value"].as<int>()));
        }
        else if (strcmp(repo["type"].as<const char*>(), "HDC2080") == 0) {
            this->sensors.push_back(new Sensor_HDC2080(repo["value"].as<int>()));
        }
        else if (strcmp(repo["type"].as<const char*>(), "BMP280") == 0) {
            this->sensors.push_back(new Sensor_BMP280(repo["value"].as<int>()));
        }
        else if (strcmp(repo["type"].as<const char*>(), "DOOR") == 0) {
            this->sensors.push_back(new Sensor_DOOR(repo["value"].as<int>()));
        }
        else if (strcmp(repo["type"].as<const char *>(), "PT100") == 0) {
          this->sensors.push_back(new Sensor_PT100(2, repo["value"].as<int>() ));
        }
    }

    this->cold  = this->set_actuator(doc["cold"]["type"].as<const char*>(),doc["cold"]["value"].as<int>(),   "cold", doc["cold"]["invert"].as<bool>());
    this->heat  = this->set_actuator(doc["heat"]["type"].as<const char*>(),doc["heat"]["value"].as<int>(),   "heat", doc["heat"]["invert"].as<bool>());
    this->fan   = this->set_actuator(doc["fan"]["type"].as<const char*>(),doc["fan"]["value"].as<int>(),     "fan", doc["fan"]["invert"].as<bool>());
    this->light = this->set_actuator(doc["light"]["type"].as<const char*>(),doc["light"]["value"].as<int>(), "light", doc["light"]["invert"].as<bool>());

    //TODO set a variable in config ??
    if(this->cold == nullptr){
     //   this->cold->set_safety_time(INTERVAL_SAFETY_RELAY);
    }

    for (JsonObject repo : doc["schedule"].as<JsonArray>()) {
        this->schedule.push_back(new Timetable(repo["start"].as<const char*>(), repo["end"].as<const char*>(), repo["value"].as<float>()));
    }

    if(doc.containsKey("pid")){
        JsonObject pid_values = doc["pid"].as<JsonObject>();
        m_PID_Kp = pid_values["Kp"].as<float>();
        m_PID_Ki = pid_values["Ki"].as<float>();
        m_PID_Kd = pid_values["Kd"].as<float>();
        m_PID_maxIntegralError = pid_values["maxIntegralError"].as<float>();
        m_PID_maxDiffError = pid_values["maxDiffError"].as<float>();
    } else {
        m_PID_Kp = 1.0f;
        m_PID_Ki = 0.01f;
        m_PID_Kd = 30.0f;
        m_PID_maxIntegralError = 4001.0f;
        m_PID_maxDiffError = 3.0f;
    }

    //TODO send all data of configuration loaded to mqtt server (retain)

}

Actuator* Config::set_actuator(String type, int value, String name, bool invert = false)
{
    if(type == "RELAY"){
        return new Relay(value, name, invert);
    }
    else if (type == "SOLID") {
        return new Solid(value, name, invert);
    }
    else if (type == "PWM") {
        return new SoftPwm(value, name, invert);
    }


    return nullptr;
}

MODE Config::get_mode() {
    return this->m_mode;
}
HANDLER_TYPE Config::get_handler(){
    return m_handler;
}

String Config::get_mode_string() {
    switch (m_mode)
    {
        case MODE::SETUP:
            return "setup";
        case MODE::NORMAL:
            return "normal";
    }
    return "unknow";
}

String Config::get_handler_string() {
    switch (m_handler)
    {
        case HANDLER_TYPE::TEST:
            return "test";
        case HANDLER_TYPE::BASIC:
            return "basic";
        case HANDLER_TYPE::PID:
            return "pid";
    }
    return "unknow";
}

String Config::get_ntp_server() {
    return this->m_ntp_server;
}

long int Config::get_ntp_gmt_offset() {
    return this->m_ntp_gmt_offset;
}

long int Config::get_ntp_daylight_offset() {
    return this->m_ntp_daylight_offset;
}

String Config::get_wifi_ssid() {
    return this->m_wifi_ssid;
}

String Config::get_wifi_password() {
    return this->m_wifi_password;
}

float Config::get_temperature_reference(){
    for (auto timetable : this->schedule)
    {
        if(timetable->is_in_interval()){
            return timetable->get_value();
        }
    }
    //TODO send alert, error in schedule
    return TEMP_ERROR_READING;
}

MODE decode_json_key_as_mode(JsonDocument &doc, const char *key, MODE default_value) {
    if (doc.containsKey(key) && doc[key].is<const char *>()) {
        if (strcmp(doc[key].as<const char*>(), "setup") == 0) {
            return MODE::SETUP;
        }
        else {
            return MODE::NORMAL;
        }
    }
    return default_value;
}

HANDLER_TYPE decode_json_key_as_handler(JsonDocument& doc, const char* key, HANDLER_TYPE default_value) {
    if (doc.containsKey(key) && doc[key].is<const char*>()) {
        if (strcmp(doc[key].as<const char*>(), "test") == 0) {
            return HANDLER_TYPE::TEST;
        }else if (strcmp(doc[key].as<const char*>(), "basic") == 0) {
            return HANDLER_TYPE::BASIC;
        }else if (strcmp(doc[key].as<const char*>(), "pid") == 0) {
            return HANDLER_TYPE::PID;
        }
    }
    return default_value;
}

long int decode_json_key_as_long(JsonDocument &doc, const char *key, long int default_value) {
    long value = default_value;
    if (doc.containsKey(key) && doc[key].is<long int>()) {
        value = doc[key].as<long int>();
    }
    return value;
}

String decode_json_key_as_name(JsonDocument &doc, const char *key, const char* default_value) {
    if (doc.containsKey(key) && doc[key].is<const char *>()) {
        return doc[key].as<const char *>();
    }
    if (strcmp(default_value, "") != 0) {
        return default_value;
    }
    return "";
}

}
