#include "webserver.h"
#include "actuator.h"

namespace SeedBank{
// for send mqtt data
extern WebServer* webserver;

bool Actuator::is_active() {
    return m_is_active;
}

String Actuator::get_type() {
    switch (m_type)
    {
        case ACTUATOR_TYPE::RELAY:
            return "RELAY";
        case ACTUATOR_TYPE::SOLID:
            return "SOLID";
        case ACTUATOR_TYPE::PWM:
            return"PWM";

        default:
         return "unknow";
    }

}

void Actuator::send_state()
{
    if (webserver != nullptr && !m_last_change_published) {
        //TODO send message as retain ??
        if (webserver->mqtt_publish("actuator/" + m_name, String(m_value, 2))) {
            m_last_change_published = true;
        }
    }
}

float Actuator::get_value(){
    return static_cast<float>(this->is_active());
}

void Actuator::set_value(float value)
{
    if(value > 0.01f){
        this->enable();
    }else{
        this->disable();
    }
}

/*
 *   RELAY
 */
Relay::Relay(int pin, String name, bool invert) {
    this->m_pin = pin;
    this->m_name = name;
    this->m_is_invert = invert;
    this->m_type = ACTUATOR_TYPE::RELAY;
    pinMode(this->m_pin, OUTPUT);
    this->disable();
}

void Relay::enable() {
    auto currentTime = millis();
    if( (currentTime - m_last_changed_state_time) > m_safety_time_millis){

        if (!m_is_active) {
            m_last_changed_state_time = currentTime;
            this->m_is_active = true;
            m_value = 1.0f;
            m_last_change_published = false;
            this->send_state();
        }
        if(this->m_is_invert){
            digitalWrite(this->m_pin, HIGH);
        }else{
            digitalWrite(this->m_pin, LOW);
        }

    }else{
        //TODO send message ??
        Serial.println("RELAY BLOCKED can't enabled");
    }
}

void Relay::disable() {
    auto currentTime = millis();
    if ((currentTime - m_last_changed_state_time) > m_safety_time_millis) {

        if (m_is_active) {
            m_last_changed_state_time = currentTime;
            this->m_is_active = false;
            m_value = 0.0f;
            m_last_change_published = false;
            this->send_state();
        }
        if(this->m_is_invert){
            digitalWrite(this->m_pin, LOW);
        }else{
            digitalWrite(this->m_pin, HIGH);
        }
    }else{
        //TODO send message ??
        Serial.println("RELAY BLOCKED can't disable");
    }
}

/*
 *   SOLID
 */
Solid::Solid(int pin, String name, bool invert) {
    this->m_pin = pin;
    this->m_name = name;
    this->m_is_invert = invert;
    this->m_type = ACTUATOR_TYPE::SOLID;
    pinMode(this->m_pin, OUTPUT);
    this->disable();
}

void Solid::enable() {
    if (!m_is_active) {
        this->m_is_active = true;
        m_value = 1.0f;
        m_last_change_published = false;
        this->send_state();
    }
    if(this->m_is_invert){
        digitalWrite(this->m_pin, HIGH);
    }else{
        digitalWrite(this->m_pin, LOW);
    }

}

void Solid::disable() {
    if (m_is_active) {
        this->m_is_active = false;
        m_value = 0.0f;
        m_last_change_published = false;
        this->send_state();
    }

    if(this->m_is_invert){
        digitalWrite(this->m_pin, LOW);
    }else{
        digitalWrite(this->m_pin, HIGH);
    }

}



/*
 *   PWM by software
 */

SoftPwm::SoftPwm(int pin, String name, bool invert) {
    this->m_pin = pin;
    this->m_name = name;
    this->m_is_invert = invert;
    this->m_type = ACTUATOR_TYPE::PWM;

    pinMode(pin, OUTPUT);
    m_pwmTickTime = 0;

    xTaskCreate(
        loop_softPWM_task,  // Task function.
        "softPWM",          // String with name of task.
        1000,              // Stack size in bytes.
        (void*)this,        // Parameter passed as input of the task
        1,                  // Priority of the task.
        &this->m_task);     // Task handle.

    this->disable();
}

void loop_softPWM_task(void* p_softPWM) {
    SoftPwm* softPwm = (SoftPwm*)p_softPWM;
    unsigned long currentMicros = micros();
    unsigned long previousMicros = 0;

    while (true) {
        if(softPwm->is_active() || softPwm->get_state_pin()){
            currentMicros = millis();
            softPwm->handlePWM(currentMicros - previousMicros);
            previousMicros = currentMicros;
        }
        vTaskDelay((softPwm->frequency()) / portTICK_PERIOD_MS);
    }
}

void SoftPwm::handlePWM(unsigned long deltatime) {

    m_pwmTickTime += deltatime;

    float step = min(1.0f, ((float)m_pwmTickTime / (float)interval_time));  // between 0 and 1

    m_state_pin = (step <= m_value);
    if(this->m_is_invert){
        digitalWrite(m_pin, !m_state_pin);
    }else{
        digitalWrite(m_pin, m_state_pin);
    }


    if (m_pwmTickTime > interval_time) {
        m_pwmTickTime -= interval_time;
    }
}

void SoftPwm::set_value(float value)
{
    auto previous = m_value;
    value = max(0.0f, min(1.0f, value));
    if (previous != value) {
        m_value = value;
        m_is_active = (value > 0.001f);
        m_last_change_published = false;
        this->send_state();
    }
}

float SoftPwm::get_value()
{
    return m_value;
}

void SoftPwm::enable() {
    set_value(1);
}

void SoftPwm::disable() {
    set_value(0);
}



/*
 *  REAL PWM
 */
int number_pwm_used = 0;

Pwm::Pwm(int pin, String name, bool invert) {
    this->m_pin = pin;
    this->m_name = name;
    this->m_is_invert = invert;
    this->m_type = ACTUATOR_TYPE::PWM;

    ledcAttachPin(pin, m_pwm_channel + number_pwm_used);
    ledcSetup(m_pwm_channel, m_pwm_freq, m_pwm_res);

    number_pwm_used++;
    this->disable();
}

void Pwm::set_value(float value)
{
    auto previous = m_value;
    value = max(0.0f, min(1.0f, value));
    int steps = pow(2, m_pwm_res);
    float duty_cycle = value * steps-1;

    if (previous != value) {
        m_value = value;
        m_is_active = (value > 0.001f);
        m_last_change_published = false;
        this->send_state();
    }

    //8 bit resolution -> 256 steps (2^8)
    ledcWrite(m_pwm_channel, duty_cycle);
}

float Pwm::get_value()
{
    return m_value;
}

void Pwm::enable() {
    this->set_value(1);
}

void Pwm::disable() {
    this->set_value(0);
}


ACTUATOR_TYPE getActuatorType(String type){
    if(type == "RELAY"){
        return ACTUATOR_TYPE::RELAY;
    }
    else if (type == "SOLID") {
        return ACTUATOR_TYPE::SOLID;
    }
    else if (type == "PWM") {
        return ACTUATOR_TYPE::PWM;
    }

    return ACTUATOR_TYPE::NONE;
}
}
