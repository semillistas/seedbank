#include "state.h"
#include "sensor.h"

namespace SeedBank {
State::State(Config *config) {
    SERIAL_PRINTLN("Setup State");
    this->m_config = config;
    // TODO
}

float State::get_avg_temperature() {
    float temperatures = 0.0;
    int count = 0;
    for (Sensor *s : this->m_config->sensors) {
        if(s->is_temperature()){
            float t = s->get_value();
            if (t != TEMP_ERROR_READING) {
                temperatures += t;
                count += 1;
            }
        }
    }
    if (count > 0) {
        return temperatures / count;
    }
    return TEMP_ERROR_READING;
}

void State::sensors_update() {
    for (Sensor* s : this->m_config->sensors) {
        s->update();
    }
}

void State::actuators_update()
{
    // send actuators initial state
    if (m_config->heat  != nullptr) m_config->heat->send_state();
    if (m_config->cold  != nullptr) m_config->cold->send_state();
    if (m_config->fan   != nullptr) m_config->fan->send_state();
    if (m_config->light != nullptr) m_config->light->send_state();
}

void State::print_all_temperatures() {
#ifdef DEBUG_SERIAL
    SERIAL_PRINT("[");
    for (Sensor *s : this->m_config->sensors) {
        if (s->is_temperature()) {
            SERIAL_PRINT(s->get_value());
            SERIAL_PRINT(", ");
        }
    }
    SERIAL_PRINT("]");
#endif
}
}
