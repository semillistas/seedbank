# SeedLab

Open source machines for seed biotecnology's laboratory.

Precise temperature control by means of a modified domestic oven.

## Esquema general de funcionamiento

![Esquema](doc/esquema de funcionamiento horno.jpg "Esquema general de funcionamiento"){width=600}

## Instalation

* [Hardware](doc/hardware.md)
* [Platformio](doc/platformio.md)
* conectar la ESP32 al resto del hardware.
* conectarse con el PC a la red wifi de la ESP32: 'SeedBank Setup'
* entrar en la IP por defecto http://192.168.1.1  para realizar cambios del setup siguiendo los pasos: [setup](doc/setup.md)


## Otras consideraciones

### web de la ESP32

La web de la ESP32 tiene tres páginas:
- "info" : tiene datos de lo que está ocurriendo en el horno.
- "setup" : explicado en los apartados anteriores
- "update" : para realizar actualizaciones ya compiladas en las que hay que introducir los archivos firmware.bin y spiffs.bin


### Averiguar la IP de la ESP32

* una vez configurado el "setup", el router del laboratorio asignará una IP a la ESP32.
* para poder acceder a la web de la ESP32 necesitamos averiguar esa IP.
* Posibles métodos:

1.- desde el terminal de linux:
 sudo nmap -sn 192.168.1.0/24
 esto busca las 255 posibles IP conectadas al router que están en la subred 192.168.1.0

2.- apps. Por ejemplo, desde un Smartphone Android: Port Authority (Descargar de Play Store)
pinchar en "discover host"

De todas las IP localizadas, una de ellas es la IP del horno. Probar hasta entrar en la web de la ESP32.
Es posible que en el futuro, el router asigne una nueva IP a la ESP32 y sea necesario volverla a averiguar.


### monitoreo con MQTT explorer

* si se ha configurado en el setup el apartado del MQTT server, previamente se habrá instalado un servidor de MQTT (mosquitto o similar).
* instalar MQTT explorer en el PC
* con ello se podrá visualizar la temperatura en tiempo real, además de otros parametros que la ESP32 envía al servidor.


## Método de trabajo con el horno

* Poner los selectores giratorios de manera que:
  pueda alcanzar una temperatura mayor de la que queremos.
  sólo funcione la resistencia inferior.

* Encender la ESP32.

* Encender el ventilador. El encendido del interruptor del ventilador nos servirá para mover el aire cuando esté en funcionamiento normal. Si tenemos que estar abriendo y cerrando la puerta del horno para sacar las bandejas de semillas, apagaremos el ventilador para no perder el calor interno.

* Abrir la web de la ESP32, en “setup”, para especificar la temperatura de funcionamiento y los parámetros PID.

* Abrir el MQTT explorer en un PC del laboratorio para corroborar el buen funcionamiento.
